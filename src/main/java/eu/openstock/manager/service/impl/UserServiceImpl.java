package eu.openstock.manager.service.impl;

import eu.openstock.manager.model.User;
import eu.openstock.manager.repository.RoleRepository;
import eu.openstock.manager.repository.UserRepository;
import eu.openstock.manager.service.UserService;
import java.util.HashSet;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private RoleRepository roleRepository;


  @Override
  public Optional<User> save(User user) {

    Optional<User> existing = userRepository.findByUsername(user.getUsername());
    existing.ifPresent(it -> {
      throw new IllegalArgumentException("user already exists: " + it.getUsername());
    });

    user.setPassword(user.getPassword());
    user.setRoles(new HashSet<>(roleRepository.findAll()));
    userRepository.save(user);
    return Optional.of(user);
  }

  @Override
  public Optional<User> findByUsername(String username) {
    return userRepository.findByUsername(username);
  }

  @Override
  public Optional<User> findByUsernameOrEmail(String username, String email) {
    return userRepository.findByUsernameOrEmail(username, email);
  }

  @Override
  public Optional<User> findUserByEmail(String email) {
    return userRepository.findUserByEmail(email);
  }

  @Override
  public Optional<User> findById(Long id) {
    return userRepository.findById(id);
  }

  @Override
  public boolean existsByUsername(String username) {
    return userRepository.existsByUsername(username);
  }

  @Override
  public boolean existsByEmail(String email) {
    return userRepository.existsByEmail(email);
  }
}
