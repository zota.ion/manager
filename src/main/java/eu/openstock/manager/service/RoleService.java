package eu.openstock.manager.service;

import eu.openstock.manager.common.RoleName;
import eu.openstock.manager.model.Role;
import java.util.Optional;

public interface RoleService {

  Optional<Role> findByName(RoleName roleName);
}
