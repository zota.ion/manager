package eu.openstock.manager.graphql.publisher;

import eu.openstock.manager.model.User;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.springframework.stereotype.Component;

@Component
public class UserPublisher {

  private final Flowable<User> publisher;
  private Queue<User> mutations = new ConcurrentLinkedQueue<User>();

  /**
   * UserPublisher.
   */
  public UserPublisher() {

    Observable<User> stockPriceUpdateObservable = Observable.create(emitter -> {

      ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
      executorService.scheduleAtFixedRate(newPublisherTick(emitter), 0, 2, TimeUnit.SECONDS);

    });

    ConnectableObservable<User> connectableObservable = stockPriceUpdateObservable.share()
        .publish();
    connectableObservable.connect();

    publisher = connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
  }

  private Runnable newPublisherTick(ObservableEmitter<User> emitter) {
    return () -> {
      List<User> users = getUserMutations();
      if (!users.isEmpty()) {
        emitNotifications(emitter, users);
      }
    };
  }

  private void emitNotifications(ObservableEmitter<User> emitter, List<User> users) {

    for (User user : users) {
      try {
        emitter.onNext(user);
      } catch (RuntimeException e) {
        System.out.println(e.getMessage());
      }
    }

  }


  private List<User> getUserMutations() {

    List<User> users = new LinkedList<User>();
    while (!mutations.isEmpty()) {
      users.add(mutations.poll());
    }

    return users;
  }

  public void logMutation(User u) {

    mutations.offer(u);
  }

  /**
   * getPublisher.
   * @param firstName in
   * @return Folowable of User's
   */
  public Flowable<User> getPublisher(String firstName) {

    return publisher.filter(stockPriceUpdate -> firstName.contains(stockPriceUpdate.getUsername()));
  }

  /**
   * getPublisher.
   * @return Folowable of User'
   */
  public Flowable<User> getPublisher() {

    return publisher;

  }

}