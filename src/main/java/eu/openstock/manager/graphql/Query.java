package eu.openstock.manager.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import eu.openstock.manager.model.User;
import eu.openstock.manager.service.UserService;
import java.util.Optional;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Query implements GraphQLQueryResolver {


  private UserService userService;


  public Query() {

  }

  public Optional<User> findUserById(long id) {

    return userService.findById(id);
  }

  public Optional<User> findUserByEmail(String email) {
    return userService.findUserByEmail(email);
  }


}
