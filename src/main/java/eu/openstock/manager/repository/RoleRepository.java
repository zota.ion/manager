package eu.openstock.manager.repository;

import eu.openstock.manager.model.Role;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

  Optional<Role> findRoleByName(String roleName);

}
