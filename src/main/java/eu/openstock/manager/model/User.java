package eu.openstock.manager.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "users")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @NotBlank
  @Size(max = 100)
  private String username;
  @NotBlank
  @Size(max = 100)
  private String firstName;
  @NotBlank
  @Size(max = 100)
  private String lastName;
  @NotBlank
  @Size(max = 100)
  private String email;
  @NotBlank
  @Size(max = 100)
  @Column(name = "password")
  private String password;
  @ManyToMany
  @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "users.id"),
      inverseJoinColumns = @JoinColumn(name = "roles.id"))
  private Set<Role> roles = new HashSet<>();


}
